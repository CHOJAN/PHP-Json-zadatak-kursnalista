-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 18, 2018 at 02:00 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kursna_lista`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzija_valute`
--

DROP TABLE IF EXISTS `konverzija_valute`;
CREATE TABLE IF NOT EXISTS `konverzija_valute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iznos` float NOT NULL,
  `valuta` varchar(20) NOT NULL,
  `iznos_kursa` float NOT NULL,
  `iznos_konverzije` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzija_valute`
--

INSERT INTO `konverzija_valute` (`id`, `timestamp`, `iznos`, `valuta`, `iznos_kursa`, `iznos_konverzije`) VALUES
(20, '2018-06-17 22:00:00', 250, 'euro', 118.52, 29630),
(21, '2018-06-18 11:49:12', 320, 'kanadski dolar', 77.369, 24758.1),
(22, '2018-06-18 11:53:41', 100, 'americki dolar', 100.065, 10006.5),
(23, '2018-06-18 11:57:42', 220, 'britanska funta', 134.021, 29484.6),
(24, '2018-06-18 11:58:37', 350, 'britanska funta', 133.619, 46766.6),
(25, '2018-06-18 11:59:02', 580, 'australijski dolar', 75.912, 44029),
(26, '2018-06-18 11:59:34', 260, 'japanski jen', 0.908416, 236.188);

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta` int(11) NOT NULL,
  `kupovni` float NOT NULL,
  `srednji` float NOT NULL,
  `prodajni` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, '2018-06-17', 1, 117.813, 118.165, 118.52),
(2, '2018-06-17', 2, 101.257, 101.562, 101.867),
(3, '2018-06-17', 3, 133.619, 134.021, 134.425),
(4, '2018-06-17', 4, 99.765, 100.065, 100.365),
(5, '2018-06-17', 5, 0.905691, 0.908416, 0.911141),
(6, '2018-06-17', 6, 1.593, 1.5978, 1.6026),
(7, '2018-06-17', 7, 75.4579, 75.685, 75.912),
(8, '2018-06-17', 8, 76.906, 77.137, 77.369),
(9, '2018-06-17', 9, 11.5908, 11.6527, 11.6607);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) NOT NULL,
  `kod` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'euro', 'eur'),
(2, 'svajcarski franak', 'chf'),
(3, 'britanska funta', 'gbp'),
(4, 'americki dolar', 'usd'),
(5, 'japanski jen', 'jpy'),
(6, 'rublja', 'rub'),
(7, 'australijski dolar', 'aud'),
(8, 'kanadski dolar', 'cad'),
(9, 'svedska kruna', 'sek');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
