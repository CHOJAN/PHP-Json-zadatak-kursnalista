<?php

include('db.php');

$iznos = $_POST['iznos'];
$valuta = $_POST['valuta'];
$kurs = $_POST['kurs'];

$sql = "SELECT " . $kurs . " FROM kurs_dinara WHERE valuta='". $valuta ."'";

$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

foreach($result AS $res){

  $vrednost = $res[$kurs];

}

$sql_valuta = "SELECT naziv FROM valuta WHERE id='". $valuta ."'";

$result_valuta = mysqli_query($connection,$sql_valuta) or die(mysqli_error($connection));

foreach($result_valuta AS $res_valuta){

  $valuta_naziv = $res_valuta['naziv'];

}

$konv_iznos = $iznos*$vrednost;
$date = date_format(date_create(), 'Y-m-d');
//$timestamp = date('H:i:s');
$timestamp = date_format(date_create(), 'Y-m-d H:i:s');


$sql_convert = "INSERT INTO konverzija_valute(timestamp,iznos,valuta,iznos_kursa,iznos_konverzije)
        VALUES ('$timestamp' ,'$iznos','$valuta_naziv','$vrednost','$konv_iznos')";

$result = mysqli_query($connection,$sql_convert) or die(mysqli_error($connection));

?>

<html>
  <head>
  <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
    <div class="main">
      <h3>Konvertor valute:</h3><br>
      Konvertovana valuta:<span> <?php echo($valuta_naziv) ?></span><br>
      Iznos za konverziju:<span> <?php echo($iznos)?></span><br>
      Današnji <?php echo($kurs)?> kurs: <span> <?php echo($vrednost)?></span><br>
      Konvertovani iznos u dinarima: <span><?php echo($konv_iznos)?></span><br><br>
      <a href="index.php">Povratak na konvertor</a><br><br>
      <a href="convert_json.php">Konvertovani podaci</a>
    </div>
  </body>
</html>
