<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="styles.css">
  <title>Exchange office</title>
</head>
<body>
  

<?php include('db.php'); ?>
<div class="main">
<form action="convert.php" method="POST" enctype="multipart/form-data">
    <h2>Konvertor valuta</h2>
    <label for="amount">Iznos</label><br>
    <input type="text" name="iznos" value="" required="required"><br><br>
    <label for="currency">Valuta</label><br>
    <select type="text" name="valuta"  value="" required="required">
      <option value="">- Choose -</option>";
      <?php

      $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
      $result = mysqli_query($connection,$sql) or die(mysql_error());

      if (mysqli_num_rows($result)>0) {
        while ($record = mysqli_fetch_array($result,MYSQLI_BOTH))
          echo "<option value=\"$record[id]\">$record[naziv]</option>";
      }

      ?>
    </select><br>
    <label for="currency-type">Odaberite kurs</label><br>
    <input class="currency" type="radio" name="kurs" value="kupovni" checked> Kupovni<br>
    <input class="currency" type="radio" name="kurs" value="srednji"> Srednji<br>
    <input class="currency" type="radio" name="kurs" value="prodajni"> Prodajni
    <br><br>
    <input type="submit" name="convert" class="convert" value="Konvertuj">
</form>
</div>

</body>
</html>